// 拿到当前网络的ip地址
const os = require('os')
var ifaces = os.networkInterfaces();
	
let networkAddress =( (ifaces["en0"] || []).filter(item => item.family === "IPv4")[0] || {}).address || 'noAddress';

// 设置基本信息端口，以及拼接api接口地址
const PORT = 2233
const localURL = `http://localhost:${PORT}`
const networkURL = `http://${networkAddress}:${PORT}`

module.exports = {
	PORT, localURL, networkURL
}