const mongoose = require('../index.js');

const AuthSchema = new mongoose.Schema({
	role: {
		type: String,
		unique: true,
		required: true,
	},
	menuList: {
		type: Array,
		default: [
			{ id: 1, title: "首页", url: "/home", icon: "el-icon-s-home", level: 1 },
			{
				id: 3,
				title: "系统管理",
				url: "/iconManage",
				icon: "el-icon-s-tools",
				level: 1,
				children: [
					{
						id: 31,
						title: "图标管理",
						url: "/iconManage",
						icon: "el-icon-s-grid",
						level: 2
					},
					{
						id: 32,
						title: "数据战报",
						url: "/dataReport",
						icon: "el-icon-s-data",
						level: 2
					}
				]
			}
		]
	}
})

module.exports = mongoose.model('Auth', AuthSchema, 'auth');