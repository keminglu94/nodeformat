// 新增、修改数据
const save = model => {
	return new Promise((resolve, reject) => {
		model.save((err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

// 查询符合条件的全部数据
const query = (model, query) => {
	return new Promise((resolve, reject) => {
		model.find(query, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

// 查询符合条件的第一个数据
const hasOne = (model, query = {}) => {
	return new Promise((resolve, reject) => {
		model.findOne(query, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

// 删除符合条件的第一个数据
const deleteOne = (model, query) => {
	return new Promise((resolve, reject) => {
		model.deleteOne(query, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

// 删除符合条件的全部数据
const deleteMany = (model, query) => {
	return new Promise((resolve, reject) => {
		model.deleteMany(query, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

// 更新符合条件的第一个数据
const updateOne = (model, query, updateData) => {
	return new Promise((resolve, reject) => {
		model.findOneAndUpdate(query, updateData, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

module.exports = {
	save,
	query,
	hasOne,
	deleteOne,
	deleteMany,
	updateOne,
}


