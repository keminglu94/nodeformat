const Koa = require('koa')
const bodyParser = require('koa-bodyparser')

const { PORT, localURL, networkURL } = require('./config.js')
//引入路由
const home = require('./router')
const user = require('./router/user') // 用户模块
const auth = require('./router/auth') // 权限模块
const file = require('./router/file') // 文件模块

const app = new Koa()
app.use(bodyParser())

app.use(async (ctx, next) => {
	// 解决跨域
	ctx.set('Access-Control-Allow-Origin', '*');
	// 对于后端-解决options请求，对于前端-解决post请求跨域问题
	ctx.set('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With');
	// ctx.set('Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS');

	if (ctx.request.method === 'OPTIONS') {
		// 解决options请求，状态码给200，然后才能往下走到post请求里面
		ctx.body = 200
	} else {
		// 放开其他请求
		await next()
	}

	console.log(`Method：${ctx.request.method}    Url：${ctx.request.url}`);
	if (parseInt(ctx.status) === 404) {
		ctx.response.redirect('/404')
	}
})

//挂载路由
app.use(home.routes())
app.use(user.routes())
app.use(auth.routes())
app.use(file.routes())

app.listen(PORT)

console.log(`  
   Server running at:
   - Local:   ${localURL}
   - Network: ${networkURL}
`);
