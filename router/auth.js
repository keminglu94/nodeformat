const router = require('koa-router')()
const { successFormat, errorFormat } = require('../common/dataFormat.js')
const { query, save, updateOne, deleteOne, hasOne } = require('../dbs/sql.js')
const AuthModel = require('../dbs/model/auth.js')

// 根据 role 获取对应的权限
router.post('/auth/getAuth', async (ctx, next) => {
	let { role } = ctx.request.body

	try {
		let res = await hasOne(AuthModel, { role })
		if (res) {
			ctx.response.body = { ...successFormat, data: res.menuList }
		} else {
			ctx.response.body = { ...successFormat }
		}
	} catch (error) {
		console.log('error', error);
		ctx.response.body = { ...errorFormat }
	}
})

// 修改权限
router.post('/auth/updateAuth', async (ctx, next) => {
	let { role, menuList } = ctx.request.body

	try {
		let res = await updateOne(AuthModel, { role }, { menuList })
		console.log(res);
		ctx.response.body = { ...successFormat, data: res }
	} catch {
		console.log('error', error);
		ctx.response.body = { ...errorFormat }
	}
})

// 增加权限
router.post('/auth/addAuth', async (ctx, next) => {
	let { role, menuList } = ctx.request.body
	let newAuth = new AuthModel({ role, menuList })
	try {
		let res = await save(newAuth)
		ctx.response.body = { ...successFormat, data: res }
	} catch (error) {
		console.log('error', error);
		if (error.code === 11000) {
			ctx.response.body = { ...errorFormat, message: '角色已重复' }
		} else {
			ctx.response.body = { ...errorFormat }
		}
	}
})

module.exports = router