const router = require('koa-router')()
const multer = require('@koa/multer');
const { successFormat, errorFormat } = require('../common/dataFormat.js')

// 如果你未创建文件上传需要保存的文件夹或文件，使用dest时，会根据dest配置的路径自动创建
// 但是如果使用storage，必须确保文件夹或文件是否存在，否则会报错！
// 关于路径的坑：存放文件的路径是项目的根目录
const storage = multer.diskStorage({
	// multer调用diskStorage可控制磁盘存储引擎
	destination: function (req, file, cb) {
		cb(null, './static/images/')
	},
	filename: function (req, file, cb) {
		cb(null, file.fieldname + '-' + Date.now() + '.jpg') // 文件名使用cb回调更改，参数二是文件名，为了保证命名不重复，使用时间戳
	}
})

// 指定一些数据大小（做限制的），object类型
const limits = {
	fields: 10,//非文件字段的数量
	fileSize: 500 * 1024,//文件大小 单位 b
	files: 1//文件数量
}

/*
上边file 输出：
{
	fieldname: 'file',
	originalname: 'xm.jpg',
	encoding: '7bit',
	mimetype: 'image/jpeg'
}
*/

const upload = multer({
	storage
});

// upload.single('file') 参数file是前端上传的文件字段名 element上传组件中的name		
// 注意，这个字段名前后端必须一致
router.post('/upload', upload.single('file'), async (ctx, next) => {
	ctx.response.body = {
		...successFormat
	}
})

module.exports = router