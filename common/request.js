// 封装接口 返回的是一个promise 
// 异步拿到数据用then() 
// 同步拿到数据使用 async await 
import axios from "axios";
import qs from "qs";

const { localURL: baseURL } = require('../config.js')

const ajax = axios.create({
	baseURL,
	timeout: 50000,
	headers: {
		"Content-Type": "application/json;charset=utf-8"
	}
})
export default {
	get: async (url, params, config = {}) => {
		try {

			let res = await ajax.get(url, { params, ...config })
			return res
		} catch (error) {
			throw new Error(error)
		}
	},
	post: async (url, params, config = {}) => {
		try {
			let res = await ajax.post(url, params, config)
			return res
		} catch (error) {
			throw new Error(error)
		}
	},
	postForm: async (url, params, config = {}) => {
		try {
			let res = await ajax.post(url, qs.stringify(params), {
				...config,
				headers: {
					// 请求表单数据的请求头
					"Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
				}
			})
			return res
		} catch (error) {
			throw new Error(error)
		}
	}
}