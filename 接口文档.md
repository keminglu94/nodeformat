# 文档说明

先是接口的说明，然后是接口地址+请求方式，如果有参数就会写出来，没有参数就不写。最后是响应例子。
关于前端传递参数，可以多给，但不能少给，数据校验放在前端处理。
关于报错，前端只需要提示 message 即可。

# 用户模块

## 得到全部用户

```js
	http://localhost:2233/user/list		get
	{
		"code": 200,
		"message": "",
		"success": true,
		"data": [
					{
						"_id": "616132723cb06bad48a60804",
						"username": "zhangsan",
						"password": "12345678",
						"name": "撒的撒的撒",
						"age": 20,
						"sex": "男",
						"tel": 10086110120,
						"status": 1,
						"__v": 0
					}
				]
	}
```

## 增加用户/用户注册

```js
	http://localhost:2233/user/add	post
	请求参数
	{
		username:'1234566',		// true
		password:'12345678',	// true
		name:'alice',			// true
		age:24,
		sex:'女',
		tel:'10086',
	}
	响应
	{
		"code": 200,
		"message": "",
		"success": true,
		"data": {
			"username": "1234566",
			"password": "12345678",
			"status": 1,
			"_id": "6195bffaceccd10ee596ae48",
			"__v": 0
		}
	}
```

## 用户登录

```js
	http://localhost:2233/user/login	post
	请求参数
	{
		username:'zhangsan',	// true
		password:'12345678',	// true
	}
	响应
	{
		"code": 200,
		"message": "",
		"success": true,
		"data": {
			"_id": "616132723cb06bad48a60804",
			"username": "zhangsan",
			"password": "12345678",
			"name": "撒的撒的撒",
			"age": 20,
			"sex": "男",
			"tel": 10086110120,
			"status": 1,
			"__v": 0
		}
	}
```

## 更新用户信息

```js
	http://localhost:2233/user/update	post
	请求参数
	{
		"username": "zhangsan",		// true
		"name": "张三",				// true
		"age": 20,
		"sex": "男",
		"tel": 10086110120,
	}
	响应
	{
    "code": 200,
    "message": "",
    "success": true,
    "data": {
        "_id": "616132723cb06bad48a60804",
        "username": "zhangsan",
        "password": "12345678",
        "name": "张三",
        "age": 20,
        "sex": "男",
        "tel": 10086110120,
        "status": 1,
        "__v": 0
    }
}
```

## 删除用户

```js
	http://localhost:2233/user/del	post
	请求参数
	{
		username:'zhangsan'		// true
	}
	响应
	{
		"code": 200,
		"message": "",
		"success": true,
		"data": {
			"deletedCount": 1
		}
	}
```

# 权限模块

## 获取角色的权限

```js
	http://localhost:2233/auth/getAuth	post
	请求参数
	{
		role:'user'		// true
	}
	响应
	{
		"code": 200,
    	"message": "",
    	"success": true,
    	"data": [
			{
				"id": 1,
				"title": "首页",
				"url": "/home",
				"icon": "el-icon-s-home",
				"level": 1
			},
			{
				"id": 3,
				"title": "系统管理",
				"url": "/iconManage",
				"icon": "el-icon-s-tools",
				"level": 1,
				"children": [
					{
						"id": 31,
						"title": "图标管理",
						"url": "/iconManage",
						"icon": "el-icon-s-grid",
						"level": 2
					},
					{
						"id": 32,
						"title": "数据战报",
						"url": "/dataReport",
						"icon": "el-icon-s-data",
						"level": 2
					}
				]
			}
		]
	}
```

## 更新角色路由权限

```js
	http://localhost:2233/auth/updateAuth post
	请求参数
	{
		role:'user',	// true
		menuList:[		// true
			{
				id: 3,
				title: "系统管理",
				url: "/iconManage",
				icon: "el-icon-s-tools",
				level: 1,
				children: [
					{
						id: 31,
						title: "图标管理",
						url: "/iconManage",
						icon: "el-icon-s-grid",
						level: 2
					},
					{
						id: 32,
						title: "数据战报",
						url: "/dataReport",
						icon: "el-icon-s-data",
						level: 2
					}
				]
			}
		]
	}
	响应
	{
		// 返回的是修改之前的数据
		"code": 200,
		"message": "",
		"success": true,
		"data": {
			"_id": "61bbf92110acf25c2fb4f14e",
			"role": "user",
			"menuList": [
				{
					"id": "1",
					"title": "首页",
					"url": "/home",
					"icon": "el-icon-s-home",
					"level": "1"
				}
			],
			"__v": 0
    }
}
```
